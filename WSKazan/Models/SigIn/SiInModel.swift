//
//  SiInModel.swift
//  WSKazan
//
//  Created by Nq on 28.01.2021.
//

import Foundation

struct SigInModel: Decodable{
    
    let token:String
}




class SigInMethods {
    
    
    func sigIn(email: String, password: String) -> SigInModel? {
        
        let semaphore = DispatchSemaphore (value: 0)
        var sigin:SigInModel?

        let json: [String: Any] = ["email" : "\(email)",
                                   "password" : "\(password)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        var request = URLRequest(url: URL(string: "http://wsk2019.mad.hakta.pro/api/user/login")!,timeoutInterval: Double.infinity)
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else { return }
            
            
            sigin = try? JSONDecoder().decode(SigInModel.self, from: data)
            print(sigin)
            
            semaphore.signal()
            
        }.resume()
        
        semaphore.wait()
        
        return sigin
    }
    
    
}
