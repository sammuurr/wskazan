//
//  SigInViewController.swift
//  WSKazan
//
//  Created by Nq on 27.01.2021.
//

import UIKit

class SigInViewController: UIViewController {

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func textFieldTapReturn(_ sender: UITextField) {
        view.endEditing(true)
    }
    @IBAction func sigInButton(_ sender: UIButton) {
        view.endEditing(true)
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let button = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(button)
        
        
        if !(emailTextField.hasText) {
            alert.title = "Емаил не введен"
            alert.message = "Пажалуйста введите емаил и попробуйте снова"
            
            self.present(alert, animated: true, completion: nil)
            
        }else{
            if !(passwordTextField.hasText) {
                alert.title = "Пароль не введен"
                alert.message = "Пажалуйста введите пароль и попробуйте снова"
                
                self.present(alert, animated: true, completion: nil)
                
            }else{
                let sesionToken = SigInMethods().sigIn(email: emailTextField.text!, password: passwordTextField.text!)
                
                if sesionToken == nil{
                    alert.title = "Пароль или емаил неправильны"
                    alert.message = "Пажалуйста введите пароль и емаил, попробуйте снова"
                    
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let vc = (storyboard?.instantiateViewController(withIdentifier: "main"))!
                    
                    present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
}
